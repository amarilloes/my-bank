# Imagen origen
FROM node:6.0.0

# Carpeta raiz
WORKDIR /mibanco

#Copia de archivos de la carpeta local a apitechu
ADD . /mibanco

#si en .dockerignore añadimos ./node_modules
#poner esto ()
RUN npm install

#Exponer puerto que estamos utilizando
EXPOSE 3000

#Comando de inicialización
CMD ["npm", "start"]
