var express = require('express');
var app = express();
var db = require('./db');


var UserController = require('./controllers/UserController');
app.use('/users', UserController);

var SessionController = require('./controllers/SessionController');
app.use('/sessions', SessionController);

var AccountController = require('./controllers/AccountController');
app.use('/accounts', AccountController);

var TransactionController = require('./controllers/TransactionController');
app.use('/transactions', TransactionController);

var ExternalAPIController = require('./controllers/ExternalAPIController');
app.use('/externalData', ExternalAPIController);

module.exports = app;
