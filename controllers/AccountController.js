
const crypt = require('../crypt');
const express = require('express');
var router = express.Router();
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const config = require('../config');
const VerifyToken = require('../VerifyToken');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

var Account = require('../model/Account');
var Transaction = require('../model/Transaction');

// CREATES A NEW ACCOUNT
router.post('/v1', VerifyToken ,function (req, res) {

  console.log("POST /account/v1");
  var ibanNumber =ibanGenerator();
  console.log("Creando cuenta...");

  var req_currency = req.body.currency?req.body.currency:"EUR";
  var req_dailyLimit = req.body.dailyLimit?req.body.dailyLimit:600;
  var req_transactionLimit = req.body.transactionLimit?req.body.transactionLimit:150;
  var balance = (!req.body.balance || req.body.balance<0 ) ? 0.00: req.body.balance;

        Account.create({
            iban : ibanNumber,
            userId : req.userId,
            balance : balance,
            currency: req_currency,
            dailyLimit: req_dailyLimit,
            transactionLimit: req_transactionLimit,
            alias: req.body.alias
            },
            function (err, account) {
                if (err) return res.status(500).send({"error_msg":"Se ha producido un error en la creación de la cuenta"});
                console.log("Cuenta creada");

                //crear movimiento
                if (balance>0){
                    Transaction.create({
                      amount : balance,
                      feesAmount: 0.00,
                      totalAmount: balance,
                      type: "INGRESO",
                      date : new Date(),
                      initialBalance: 0.00,
                      finalBalance: balance,
                      accountId: account._id,
                      description: "Ingreso inicial",
                      currency: account.currency
                        },
                        function (err, transaction) {
                            if (err) console.log("Error creando movimiento inicial");
                            console.log("Movimiento creado");
                    });//create
              }
              res.status(200).send(account);
        });
});

// RETURNS ALL THE ACCOUNTS IN THE DATABASE OR AN ACCOUNT BY IBAN
router.get('/v1', VerifyToken,function (req, res) {

  console.log("GET /accounts/v1");

  if (req.query.iban!=null){

    console.log("Busqueda por iban");
    Account.findOne({"iban": req.query.iban},  {__v:0}, function (err, account) {

       if (err) return res.status(500).send({"error_msg":"Ha habido un error en la búsqueda"});
       if (!account) return res.status(404).send({"error_msg":"No se ha encontrado la cuenta"});
       if (account.userId!=req.userId) return res.status(403).send({"error_msg":"Acceso no autorizado"});
       res.status(200).send(account);
     });

  }else if (req.query.userId!=null) {
    console.log("Listado de cuentas de usuario");

    if (req.query.userId!=req.userId) return res.status(403).send({"error_msg":"Acceso no autorizado"});
    Account.find({"userId": req.query.userId},  {__v:0}, function (err, accounts) {

       if (err) return res.status(500).send({"error_msg":"Ha habido un error en la búsqueda."});
       if (!accounts) return res.status(404).send({"error_msg":"No se ha encontrado la cuenta"});
       res.status(200).send(accounts);
     });
  }
});

// GETS A SINGLE ACCOUNT FROM THE DATABASE BY ID
router.get('/v1/:id', VerifyToken, function (req, res) {

  console.log("GET accounts/id")

  Account.findById(req.params.id, {__v:0}, function (err, account) {

    if (err) return res.status(500).send({"error_msg":"Ha habido un error en la búsqueda"});
    if (!account) return res.status(404).send({"error_msg":"No se ha encontrado la cuenta"});
    if (account.userId!=req.userId) return res.status(403).send({"error_msg":"Acceso no autorizado"});
    res.status(200).send(account);
  });

});

// DELETES An ACCOUNT FROM THE DATABASE
router.delete('/v1/:id',VerifyToken, function (req, res) {
  //hacer el control de canal antes de borrar
    Account.findByIdAndRemove(req.params.id, function (err, account) {
        if (err) return res.status(500).send({"error_msg":"Ha habido un problema en la baja de cuenta"});
        res.status(200).send("Cuenta "+ account.iban +" eliminada.");
    });
});

// UPDATES A SINGLE account IN THE DATABASE
router.put('/v1/:id', VerifyToken, function (req, res) {
  console.log("PUT accounts/v1/:id");

    Account.findByIdAndUpdate(req.params.id, req.body, {new: true,  __v:0}, function (err, account) {
        if (err) return res.status(500).send({"error_msg":"Ha habido un error actualizando la cuenta."});
        res.status(200).send(account);
    });
});

function ibanGenerator (){

  var country ="ES";
  var bank_branch = "0182 2205";
  var random10 = Math.floor(Math.random() * 10000000000);

  var ibanNumber= country + Math.floor(Math.random() * 100) + ' ' + bank_branch + ' ' + Math.floor(Math.random() * 100) + ' '+ random10;
  console.log("IBAN GENERADO: " +ibanNumber);
  return ibanNumber;
}

module.exports = router;
