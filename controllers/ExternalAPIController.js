
const express = require('express');
var router = express.Router();
const bodyParser = require('body-parser');
const config = require('../config');
const requestJson = require("request-json");
const curreciesBase = "http://openexchangerates.org/api/";
const convertBase = "http://neutrinoapi.com/";

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

//http://openexchangerates.org/api/currencies.json
// Devuelve listado de divisas
router.get('/currencies/v1', function (req, res) {

  console.log("GET /curencies/v1");
  //cliente http a la url base de Mlab definida
    httpClient = requestJson.createClient(curreciesBase);
    console.log("Cliente creado");

    //petición
    httpClient.get("currencies.json",
      function (err, resCurrencies, body){
        if (err){
          console.log("ERROR currencies " + err);
          res.status(500).send({"msg": "Error obteniendo divisas"});
        }
        console.log("Peticion divisas OK");
        console.log(body);
        res.status(200).send(body);
      }
    );

});

router.get('/exchange/v1', function (req, res) {

  console.log("GET /exchange/v1");

  var fromValue = req.query.fromValue;
  var fromType = req.query.fromType;
  var toType = req.query.toType;

  if (!fromValue) res.send({"error_msg":"Por favor, introduzca campos obligatorios"});
  if (!fromType) fromType="EUR";
  if (!toType) toType="USD";

    httpClient = requestJson.createClient(convertBase);
    console.log("Cliente creado");

    var params = {
      "user_id": "amarilloes",
      "api_key": "xizDBvyhmvJtjJJwikwe8PesoRIbkPwpDYepkZMRMFMruVBl",
      "from-value": fromValue,
      "from-type" : fromType,
      "to-type" : toType
    }

    console.log(params);

    //petición
    httpClient.post("convert", params,
      function (err, resConvert){
        if (err){

          res.status(500).send({"msg": "Error en la conversión"});
        }
        console.log("Peticion cambio OK");
        console.log(resConvert.body);
        var result = resConvert.body;
        res.status(200).send(resConvert.body);
      }
    );

});

module.exports = router;
