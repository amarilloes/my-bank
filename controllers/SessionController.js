const crypt = require('../crypt');
var express = require('express');
var jwt = require('jsonwebtoken');
var config = require('../config');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());


var User = require('../model/User');
var Session = require('../model/Session');

router.post('/v1', function (req, res) {

  console.log("Iniciando sesión");

  var login_email = req.body.email;
  var pass = req.body.password;
  /*login ok, creamos datos de sesión*/
  if (!login_email || !pass){
      return res.status(400).send({"error_msg":"Email y password son obligatorios"});
  }
  console.log(req.body);

  User.findOne({"email": login_email},{__v:0}, function (err, user) {

    console.log("Buscando usuario");
    if (err) return res.status(500).send({"error_msg":"Ha habido un problema en el proceso de login"});
    console.log(user);
    if (!user || !crypt.checkPassword(pass,user.password)) return res.status(404).send({"error_msg":"Email o password incorrectos"});

    Session.create({
      email : login_email,
      lastAccess : new Date()
      },
      function (err, session) {
            if (err) return res.status(500).send({"error_msg":"Ha habido un problema en el proceso de login"});
            console.log("Sesion creada correctamente");
            // create a token
            var token = jwt.sign({ id: user._id }, config.secret, {
                  expiresIn: 86400 // expires in 24 hours
                });
             return res.status(200).send({ sessionId: token, user: user});
      });
  });

});

module.exports = router;
