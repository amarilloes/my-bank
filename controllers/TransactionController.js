
const crypt = require('../crypt');
const express = require('express');
var router = express.Router();
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const config = require('../config');
const VerifyToken = require('../VerifyToken');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

var Account = require('../model/Account');
var Transaction = require('../model/Transaction');


router.get('/v1/daily', function(req,res){

  console.log("/v1/daily");

  var dateFrom = new Date();
  dateFrom.setUTCHours(0);
  dateFrom.setUTCMinutes(0);
  dateFrom.setUTCSeconds(0);

  console.log("Desde: " + dateFrom.toISOString());


  var totalHoy = 0.00;
  Transaction.find({
      accountId : req.query.accountId,
      date: {$gt : dateFrom.toISOString()}
    },
      function (err, todayTransactions) {
        if (err) {
         console.log(err);
         res.status(500).send({"error_msg":"Se ha producido un error"});
       }
        for (tran of todayTransactions) {
          if (tran.type!='INGRESO') totalHoy += tran.totalAmount;
        }
        console.log("TOTAL: "+ totalHoy);
        res.status(200).send(todayTransactions);
      });
})


// CREATES A NEW Transaction
router.post('/v1', VerifyToken ,function (req, res) {

  console.log("POST /transactions/v1");
  console.log("Creating transaction...");

  //consulta de saldo inicial y calculo saldo final

  Account.findById(req.body.accountId, {__v:0}, function (err, account) {

    //comprobar campos obligatorios por tipo de movimientos
    if (!mandatoryFields(req)) return res.status(400).send({"error_msg":"Por favor, introduzca campos obligatorios"});

    if (err) return res.status(500).send({"error_msg":"Se ha producido un error"});
    if (!account) return res.status(404).send({"error_msg":"Cuenta no encontrada"});
    if (account.userId!=req.userId) return res.status(403).send({"error_msg":"Acceso no autorizado"});

    var tAmount = parseFloat(req.body.amount);
    var fees= parseFloat(transactionFees(req.body.type,tAmount));
    var totalAmount = parseFloat(tAmount+fees).toFixed(2);

    console.log(fees);
    console.log(tAmount);
    console.log(totalAmount);

    //comprobacion de limites y de saldo en cuenta solo si no es INGRESO
    if (req.body.type!="INGRESO"){
      console.log("No es ingreso, se comprueban limites");
      if (account.balance< totalAmount ) return res.status(409).send({"error_msg":"Saldo insuficiente"});
      if (account.transactionLimit< totalAmount) return res.status(409).send({"error_msg":"El movimiento supera el límite de movimiento de  " + account.transactionLimit});
      if (account.dailyLimit< totalAmount) return res.status(409).send({"error_msg":"El movimiento supera el límite diario de  " + account.transactionLimit});

      console.log("Limite de movimiento: OK");
      //Límite diario:busqueda de movimientos y comprobación de límite
      var dateFrom = new Date();
      dateFrom.setUTCHours(0);
      dateFrom.setUTCMinutes(0);
      dateFrom.setUTCSeconds(0);

      console.log("Desde: " + dateFrom.toISOString());

      var totalHoy = 0;
      Transaction.find({
          accountId : account._id,
          date: {$gt : dateFrom.toISOString()}
        },
          function (err, todayTransactions) {
            if (err) {
             console.log(err);
             res.status(500).send({"error_msg":"Se ha producido un error"});
           }
           console.log("MOVIMIENTOS HOY: " + todayTransactions);
            for (tran of todayTransactions) {
                if (tran.type!='INGRESO') totalHoy += tran.totalAmount;
            }
            console.log("Acumulado día: " +totalHoy);
            if (totalHoy>account.dailyLimit) return res.status(409).send({"error_msg":"El movimiento supera el límite diario de " + account.dailyLimit});

            console.log("Limite diario: OK");
            var iBalance = account.balance;
            var fBalance = 0.00;

            fBalance = parseFloat(iBalance).toFixed(2) - parseFloat(totalAmount).toFixed(2);
            console.log("Saldo final: " + fBalance);

            Transaction.create({
              amount : tAmount,
              feesAmount: fees.toFixed(2),
              totalAmount: totalAmount,
              type: req.body.type,
              date : new Date(),
              initialBalance: iBalance,
              finalBalance: fBalance,
              accountId: req.body.accountId,
              description: typeDescription(req.body.type,req.body.description),
              receiverAccountIBAN: req.body.receiverAccountIBAN,
              senderAccountIBAN: account.iban,
              currency: account.currency,
              store: req.body.store
                },
                function (err, transaction) {
                    if (err) {
                      console.log(err);
                      return res.status(500).send({"error_msg":"Ha habido un problema en la creación del movimiento"});
                    }
                    console.log("Movimiento creado");
                    console.log(transaction);
                    //actualizar el saldo de la cuenta cuando se ha creado bien el movimiento
                    account.balance= fBalance;
                    Account.findByIdAndUpdate(req.body.accountId, account, {new: true,  __v:0}, function (err, updatedAccount) {
                        if (err) return res.status(500).send({"error_msg":"Ha habido un problema con la actualización del saldo en cuenta"});
                        console.log("Saldo actualizado");
                        console.log(updatedAccount.balance);
                        return res.status(200).send(transaction);
                    });
            });//create

          }); //find
      }// si no INGRESO
      else {
        console.log("INGRESO");

        var iBalance= parseFloat(account.balance);
        var fBalance=parseFloat(iBalance+tAmount).toFixed(2);

        console.log(account.balance);
        console.log(fBalance);

        console.log("Saldo final: " + fBalance);
        Transaction.create({
          amount : tAmount,
          feesAmount: 0.00,
          totalAmount: tAmount,
          type: req.body.type,
          date : new Date(),
          initialBalance: account.balance,
          finalBalance: fBalance,
          accountId: req.body.accountId,
          description: typeDescription(req.body.type,req.body.description),
          transferReceiverAccountIBAN: req.body.receiverIban,
          transferSenderAccountIBAN: req.body.senderIban,
          currency: account.currency,
          store: req.body.store
            },
            function (err, transaction) {
                if (err) return res.status(500).send({"error_msg":"Ha habido un problema en la creación del movimiento"});
                console.log("Movimiento creado");
                console.log(transaction);
                //actualizar el saldo de la cuenta cuando se ha creado bien el movimiento
                account.balance= fBalance;
                Account.findByIdAndUpdate(req.body.accountId, account, {new: true,  __v:0}, function (err, updatedAccount) {
                    if (err) return res.status(500).send({"error_msg":"Ha habido un problema con la actualización del saldo en cuenta"});
                    console.log("Saldo actualizado");
                    console.log(updatedAccount.balance);
                    return res.status(200).send(transaction);
                });
        });//create
      }

  });//busqueda cuenta
});

// RETURNS ALL THE transactions by userId IN THE DATABASE OR BY accountId
router.get('/v1', VerifyToken,function (req, res) {

  console.log("GET /transactions/v1");
  if (req.query.accountId!=null){
    console.log("Movimientos de cuentas: " + req.query.accountId);
    //control de que la cuenta pertenece al usuario conectado y autorizado
    Account.findById(req.query.accountId, {__v:0}, function (err, account) {

      if (err) return res.status(500).send({"error_msg":"Ha habido un error en la búsqueda"});
      if (!account) return res.status(404).send({"error_msg":"No se ha encontrado la cuenta"});
      if (account.userId!=req.userId) return res.status(403).send({"error_msg":"Acceso no autorizado"});
      console.log("Busqueda de movimientos por id de cuenta");
      Transaction.find({"accountId": req.query.accountId},  {__v:0}, function (err, transaction) {

         if (err) return res.status(500).send("There was a problem finding the Transaction.");
         if (!transaction) return res.status(404).send("No Transaction found.");
         res.status(200).send(transaction);
       });
    });

  }
});

// GETS A SINGLE Transaction FROM THE DATABASE BY ID
router.get('/v1/:id', VerifyToken, function (req, res) {

  console.log("GET transactions/id")

  Transaction.findById(req.params.id, {__v:0}, function (err, Transaction) {

    if (err) return res.status(500).send("There was a problem finding the Transaction.");
    if (!Transaction) return res.status(404).send("No Transaction found.");

    res.status(200).send(Transaction);
  });

});

// DELETES An Transaction FROM THE DATABASE
router.delete('/v1/:id',VerifyToken, function (req, res) {
    Transaction.findByIdAndRemove(req.params.id, function (err, Transaction) {
        if (err) return res.status(500).send("There was a problem deleting the Transaction.");
        res.status(200).send("Transaction "+ Transaction.iban +" was deleted.");
    });
});

// UPDATES A SINGLE Transaction IN THE DATABASE
router.put('/v1/:id', VerifyToken, function (req, res) {

    Transaction.findByIdAndUpdate(req.params.id, req.body, {new: true,  __v:0}, function (err, Transaction) {
        if (err) return res.status(500).send("There was a problem updating the Transaction.");
        res.status(200).send(Transaction);
    });
});

function ibanGenerator (){

  var country ="ES";
  var bank_branch = "0182 2205";
  var random10 = Math.floor(Math.random() * 10000000000);

  var ibanNumber= country + Math.floor(Math.random() * 100) + ' ' + bank_branch + ' ' + Math.floor(Math.random() * 100) + ' '+ random10;
  console.log("IBAN GENERADO: " +ibanNumber);
  return ibanNumber;
}

function transactionFees (transactionType, transactionAmount){
  if (transactionType=='TRANSFER') return transactionAmount*2/100;
  if (transactionType=='CASH') return transactionAmount*1/100;
  return parseFloat(0);
}

function mandatoryFields(req){

  console.log("Campos obligatorios");

  if (!req.body.amount || !req.body.type) {
    console.log("No amount o no type: "+ req.body.amount || !req.body.type);
    return false;
  }
  //comprobaciones por tipo de movimiento
  if (req.body.type=='TRANSFER')  {
    console.log("Transferencia: " +req.body.receiverAccountIBAN);
    return req.body.receiverAccountIBAN;
  }
  if (req.body.type=='PURCHASE') {
    console.log("Compra: " + req.body.store);
    return req.body.store;
  }

  return true;
}

function typeDescription(transactionType, description){
  if (description) return description;
  if (transactionType=='TRANSFER') return "Transferencia";
  if (transactionType=='CASH') return "Retirada de efectivo";
  if (transactionType=='PURCHASE') return "Compra";
  if (transactionType=='INGRESO') return "Ingreso";
}

module.exports = router;
