
const crypt = require('../crypt');
const express = require('express');
var router = express.Router();
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const config = require('../config');
const VerifyToken = require('../VerifyToken');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

var User = require('../model/User');
var Session = require('../model/Session');

// CREATES A NEW USER
router.post('/v1', function (req, res) {

  console.log("Filtro por email");

  var email = req.body.email;
  var first_name = req.body.first_name;
  var last_name = req.body.last_name;
  var languageId = !req.body.languageId? "ES":req.body.languageId;
  var password = req.body.password;
  var country = req.body.country;
  var city = req.body.city;
  var address = req.body.address ? req.body.address : "Sin direccion";

  if (!email || !first_name || !last_name || !password || !country || !city) return res.status(400).send({"error_msg":"Introduzca los campos obligatorios"});

  User.findOne({"email": req.body.email}, function (err, user) {

    console.log("Buscando usuario");
    if (err) return res.status(500).send({"error_msg":"Error en el proceso de registro"});
    if (user) return res.status(409).send({"error_msg":"El email ya está registrado"});

    User.create({
        first_name : first_name,
        last_name : last_name,
        email : email,
        languageId: languageId,
        password: crypt.hash(req.body.password),
        country: country,
        city:city,
        address:address
        },
        function (err, user) {
            if (err) return res.status(500).send({"error_msg":"Error en el proceso de registro"});
              //Genera una sesion para el usuario
              Session.create({
                  email : email,
                  lastAccess : new Date()
                  },
                  function (err, session) {
                        if (err) return res.status(500).send({"error_msg":"Error en el proceso de registro"});
                        console.log("Sesion creada correctamente");
                        // create a token
                        var token = jwt.sign({ id: user._id }, config.secret, {
                              expiresIn: 86400 // expires in 24 hours
                            });
                        res.status(200).send({ sessionId: token, user: user});
                  });
      });
    });//findone
});


// RETURNS ALL THE USERS IN THE DATABASE
router.get('/v1', VerifyToken,function (req, res) {

  console.log("GET /users/v1");
  if (req.query.email!=null){
    console.log("Filtro por email");
    User.findOne({"email": req.query.email},  { password: 0 ,__v:0}, function (err, user) {
       if (err) return res.status(500).send({"error_msg":"Error en la búsqueda de usuario"});
       if (!user) return res.status(404).send({"error_msg":"Usuario no encontrado"});
       res.status(200).send(user);
     });
  }else{
    console.log("Listado de usuarios");
    User.find({},  { password: 0 ,__v:0},function (err, users) {
      if (err) return res.status(500).send({"error_msg":"Error en la búsqueda de usuarios"});
        res.status(200).send(users);
      });
  }
});

// GETS A SINGLE USER FROM THE DATABASE
router.get('/v1/:id', VerifyToken, function (req, res) {

  console.log("GET users/id")

  if (req.userId!=req.params.id) res.status(403).send({"error_msg":"No autorizado"});
    User.findById(req.userId, { password: 0 , __v:0}, function (err, user) {
      if (err) return res.status(500).send({"error_msg":"Error en la búsqueda de usuario"});
      if (!user) return res.status(404).send({"error_msg":"Usuario no encontrado"});

      res.status(200).send(user);
    });

});

// DELETES A USER FROM THE DATABASE
router.delete('/v1/:id',VerifyToken, function (req, res) {
    User.findByIdAndRemove(req.params.id, function (err, user) {
        if (err) return res.status(500).send({"error_msg":"Error en la baja de usuario"});
        res.status(200).send("User "+ user.name +" was deleted.");
    });
});

// UPDATES A SINGLE USER IN THE DATABASE
router.put('/v1/:id', VerifyToken, function (req, res) {

  console.log("PUT /users/v1/:id");
  console.log("id :" + req.params.id);
  console.log("usuario :" + req.body);

  var modifiedUser = req.body;
  if (modifiedUser.password && modifiedUser.password!=""){
    modifiedUser.password = crypt.hash(req.body.password);
  }
    User.findByIdAndUpdate(req.params.id, modifiedUser, {new: true}, function (err, user) {
        if (err) return res.status(500).send({"error_msg":"Error en la actualización del usuario"});
        res.status(200).send(user);
    });
});

module.exports = router;
