
var mongoose = require('mongoose');
var AccountSchema = new mongoose.Schema({
  iban : String,
  alias: String,
  userId : String,
  balance : Number,
  currency: String,
  dailyLimit: Number,
  transactionLimit: Number,
});
mongoose.model('Account', AccountSchema);
module.exports = mongoose.model('Account');
