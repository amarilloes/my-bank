// User.js
var mongoose = require('mongoose');
var SessionSchema = new mongoose.Schema({
  userId: String,
  email : String,
  lastAccess : Date
});
mongoose.model('Session', SessionSchema);
module.exports = mongoose.model('Session');
