
var mongoose = require('mongoose');
var TransactionSchema = new mongoose.Schema({
  amount : Number,
  feesAmount: Number,
  totalAmount: Number,
  type: String,
  date : Date,
  initialBalance: Number,
  finalBalance: Number,
  accountId: String,
  description: String,
  receiverAccountIBAN: String,
  senderAccountIBAN: String,
  currency:String,
  store: String
});
mongoose.model('Transaction', TransactionSchema);
module.exports = mongoose.model('Transaction');
