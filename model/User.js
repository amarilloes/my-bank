// User.js
var mongoose = require('mongoose');
var UserSchema = new mongoose.Schema({
  first_name : String,
  last_name : String,
  email : String,
  languageId: String,
  password: String,
  country: String,
  city:String,
  address:String
});
mongoose.model('User', UserSchema);
module.exports = mongoose.model('User');
