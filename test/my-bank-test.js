const mocha = require('mocha');//framework test
const chai = require('chai');//aserciones adicionales a las que tiene por defecto mocha
const chaihttp = require('chai-http');//añadido a chai que nos permite hacer peticiones http
const jwt = require('jsonwebtoken');
const config = require('../config');
const crypt = require('../crypt');
const expect = require('chai').expect;


var User = require('../model/User');
var Session = require('../model/Session');

chai.use(chaihttp);

describe('Test de API users',
  function(){
    var sessionId;
    var userId;
    var session_id;

    before(function (done) {
      console.log("BEFORE TEST");
      //crear usuario
      User.create({
          first_name : "TestUser",
          last_name : "Test Last Name",
          email : "test@test",
          languageId: "ES",
          password: crypt.hash("1234")
          },
          function (err, user) {
              if (err) throw err;
              //crear sesion para el usuario
              console.log("User created : " + user._id);
              Session.create({
                email : "test@test",
                lastAccess : new Date()
                },
                function (err, session) {
                      if (err) throw err;
                      //generar token
                      console.log("Session created : " + session._id);
                      sessionId =jwt.sign({id: user._id}, config.secret, {
                            expiresIn: 86400 // expires in 24 hours
                          });
                      userId=user._id;
                      session_id=session._id;
                });
              userId=user._id;
        });

        done();
    })

    after(function (done) {
        console.log("AFTER TEST");
        console.log("UserID: " + userId);
        console.log("session_id: " + session_id);
        //borrado de usuario
        User.findByIdAndRemove(userId, function (err, user) {
            if (err) throw err;
            console.log("User deleted");
        });

        //borrado de session
        Session.findByIdAndRemove(session_id, function (err, session) {
            if (err) throw err;
            console.log("Session deleted");
        });
        done();
    })


    it('API users da error 403 si no se envía token',
      function(done){
        chai.request('http://localhost:3000')
        .get('/users/v1')
        .end(
          function(err,res){
            console.log("Request has finished");
            expect(res).to.have.status(403);
            console.log(res.body);
            done();
          }//funciontionend
        )//end
      }//done
    ), //it

    it('Login de usuario devuelve un token',
      function(done){
        chai.request('http://localhost:3000')
        .post('/sessions/v1')
        .send({
        	"email":"sara@email.com",
        	"password": "1234"})
        .end(
          function(err,res){
            console.log("Request has finished");
            expect(res).to.have.status(200);
            console.log(res.body);
            done();
          }//funciontionend
        )//end
      }//done
    )//, //it



  }
)//describe
